#ifndef __EM_Patterns_HH
#define __EM_Patterns_HH

// O2 Minipops rhythm box (c) DSP Synthesizers 2016
// Free for non commercial use

// http://janostman.wordpress.com

// cleaned up by Eric Matecki 2018

#include "Arduino.h"

#ifdef __MACOSX__
#include "avrX/interrupt.h"
#include "avrX/io.h"
#include "avrX/pgmspace.h"
#else
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#endif

#include "EM.Samples.h"

//BEURK:
#ifndef digitalWriteFast
#define digitalPinToPortReg( P )   (((P) >= 0 && (P) <= 7) ? &PORTD : (((P) >= 8 && (P) <= 13) ? &PORTB  : &PORTC))
#define digitalPinToBit( P )       (((P) >= 0 && (P) <= 7) ? (P)    : (((P) >= 8 && (P) <= 13) ? (P) - 8 : (P) - 14))
#define digitalWriteFast( P, V )   bitWrite( *digitalPinToPortReg(P), digitalPinToBit(P), (V) )
#endif


namespace  nEM {


//TODO: SoundSample ??
//TODO: Signed & Unsigned versions ??


struct  cPattern
{
  const uint8_t   mCount;
  const uint8_t*  mSamples;
};


#define  PATTERN_BEGIN( iName )  static  const uint8_t  iName##Bytes[] PROGMEM =
#define  PATTERN_END( iName )    ; const ::nEM::cPattern  iName = { iName##Bytes[0], iName##Bytes+1 };


//BEURK:
extern  uint16_t  gTempo;// = 3500;


class  cPatternPlayer
{
public:
  cPatternPlayer( const cPattern&  iPattern, ::nEM::cSamplePlayer**  iSamplePlayers ) :
    mPattern( iPattern.mSamples ),
    mPatternLength( iPattern.mCount ),
    mSamplePlayers( iSamplePlayers ),
    mTempoCount( 1 ),
    mStepCount( 0 )
  {
  }

  inline  void  Run()
  {
    if( !(mTempoCount--) )
    {
      mTempoCount = gTempo;
      digitalWriteFast( 13, HIGH ); //Clock out Hi
      uint8_t  trig = pgm_read_byte_near( mPattern + mStepCount++ );
      PORTC = mStepCount;              // output seq # for CD4067
      uint8_t  mask = (PIND>>2)|((PINB&3)<<6); // get mute switches
      trig &= mask;
      if( mStepCount > mPatternLength )  mStepCount = 0;
      if( mStepCount == 0 )
        digitalWriteFast( 12, HIGH ); //Reset out Hi
      else
        digitalWriteFast( 12, LOW );  //Reset out Lo

      if( trig &   1 )  mSamplePlayers[0]->Start();
      if( trig &   2 )  mSamplePlayers[1]->Start();
      if( trig &   4 )  mSamplePlayers[2]->Start();
      if( trig &   8 )  mSamplePlayers[3]->Start();
      if( trig &  16 )  mSamplePlayers[4]->Start();
      if( trig &  32 )  mSamplePlayers[5]->Start();
      if( trig &  64 )  mSamplePlayers[6]->Start();
      if( trig & 128 )  mSamplePlayers[7]->Start();
    }
    digitalWriteFast( 13, LOW ); //Clock out Lo
  }

  inline  void  Stop()
  {
    digitalWriteFast( 13, LOW ); //Clock out Lo
    digitalWriteFast( 12, LOW ); //Reset out Lo
    PORTC = 0;
    mStepCount = 0;
    mTempoCount = 1;
  }

private:
  const uint8_t*  mPattern;
  uint8_t  mPatternLength;

  ::nEM::cSamplePlayer**  mSamplePlayers;

  uint16_t  mTempoCount;
  uint8_t  mStepCount;
};


} // namespace  nEM

#endif // __EM_Patterns_HH

