// O2 Minipops rhythm box (c) DSP Synthesizers 2016
// Free for non commercial use

// http://janostman.wordpress.com

// cleaned up by Eric Matecki 2018

#ifdef __MACOSX__
#include "avrX/interrupt.h"
#include "avrX/io.h"
#include "avrX/pgmspace.h"
#else
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#endif


#ifndef cbi
#define cbi( sfr, bit )  (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi( sfr, bit )  (_SFR_BYTE(sfr) |= _BV(bit))
#endif 

// Standard Arduino Pins
#define digitalPinToPortReg( P )   (((P) >= 0 && (P) <= 7) ? &PORTD : (((P) >= 8 && (P) <= 13) ? &PORTB  : &PORTC))
#define digitalPinToDDRReg( P )    (((P) >= 0 && (P) <= 7) ? &DDRD  : (((P) >= 8 && (P) <= 13) ? &DDRB   : &DDRC))
#define digitalPinToPINReg( P )    (((P) >= 0 && (P) <= 7) ? &PIND  : (((P) >= 8 && (P) <= 13) ? &PINB   : &PINC))
#define digitalPinToBit( P )       (((P) >= 0 && (P) <= 7) ? (P)    : (((P) >= 8 && (P) <= 13) ? (P) - 8 : (P) - 14))

#define digitalReadFast( P )       bitRead( *digitalPinToPINReg(P), digitalPinToBit(P) )
#define digitalWriteFast( P, V )   bitWrite( *digitalPinToPortReg(P), digitalPinToBit(P), (V) )

const uint8_t  PS_128 = (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);


#include "EM.RingBuffer.h"
#include "O2MiniPop.Samples.h"
#include "O2MiniPop.Patterns.h"


namespace  nO2MiniPop {


volatile  uint16_t  gSFREQ;
static  ::nEM::cRingBuffer  sgRingBuffer;
    

inline
void
Setup()
{
  OSCCAL = 0xFF;

// Set up pin modes

  pinMode( 2, INPUT_PULLUP );  // Drum mute inputs
  pinMode( 3, INPUT_PULLUP );
  pinMode( 4, INPUT_PULLUP );
  pinMode( 5, INPUT_PULLUP );
  pinMode( 6, INPUT_PULLUP );
  pinMode( 7, INPUT_PULLUP );
  pinMode( 8, INPUT_PULLUP );
  pinMode( 9, INPUT_PULLUP );

  pinMode( 10, INPUT_PULLUP ); // RUN - Stop input

  pinMode( 11, OUTPUT );       // 8-bit PWM DAC pin

  pinMode( 12, OUTPUT );       // Reset output
  pinMode( 13, OUTPUT );       // Clock output

  pinMode( 14, OUTPUT );       // SEQ cnt output
  pinMode( 15, OUTPUT );       //      "
  pinMode( 16, OUTPUT );       //      "
  pinMode( 17, OUTPUT );       //      "

// Set up Timer 1 to send a sample every interrupt.

  cli();
  // Set CTC mode
  // Have to set OCR1A *after*, otherwise it gets reset to 0!
  TCCR1B = (TCCR1B & ~_BV(WGM13)) | _BV(WGM12);
  TCCR1A = TCCR1A & ~(_BV(WGM11) | _BV(WGM10));    
  // No prescaler
  TCCR1B = (TCCR1B & ~(_BV(CS12) | _BV(CS11))) | _BV(CS10);
  // Set the compare register (OCR1A).
  // OCR1A is a 16-bit register, so we have to do this with
  // interrupts disabled to be safe.
  //OCR1A = F_CPU / SAMPLE_RATE; 
  // Enable interrupt when TCNT1 == OCR1A
  TIMSK1 |= _BV(OCIE1A);   
  sei();
  OCR1A = 800; //40KHz Samplefreq

// Set up Timer 2 to do pulse width modulation on D11

  // Use internal clock (datasheet p.160)
  ASSR &= ~(_BV(EXCLK) | _BV(AS2));

  // Set fast PWM mode  (p.157)
  TCCR2A |= _BV(WGM21) | _BV(WGM20);
  TCCR2B &= ~_BV(WGM22);

  // Do non-inverting PWM on pin OC2A (p.155)
  // On the Arduino this is pin 11.
  TCCR2A = (TCCR2A | _BV(COM2A1)) & ~_BV(COM2A0);
  TCCR2A &= ~(_BV(COM2B1) | _BV(COM2B0));
  // No prescaler (p.158)
  TCCR2B = (TCCR2B & ~(_BV(CS12) | _BV(CS11))) | _BV(CS10);

  // Set initial pulse width to the first sample.
  OCR2A = 128;

  //set timer0 interrupt at 61Hz
  TCCR0A = 0;// set entire TCCR0A register to 0
  TCCR0B = 0;// same for TCCR0B
  TCNT0  = 0;//initialize counter value to 0
  // set compare match register for 62hz increments
  OCR0A = 255;// = 61Hz
  // turn on CTC mode
  TCCR0A |= (1 << WGM01);
  // Set CS01 and CS00 bits for prescaler 1024
  TCCR0B |= (1 << CS02) | (0 << CS01) | (1 << CS00);  //1024 prescaler 

  TIMSK0 = 0;

// Set up the ADC

  gSFREQ = analogRead(0); //EM: what's this for ?!?! gSFREQ is never used
  ADCSRA &= ~PS_128;  // remove bits set by Arduino library
  // Choose prescaler PS_128.
  ADCSRA |= PS_128;
  ADMUX = 64;
  sbi( ADCSRA, ADSC );
}


inline
void
Loop()
{
  uint8_t  MUX = 3; // start at 3 => first AD conversion will be ignored...

  for(;;)
  {
    //------------------------------------------------------- Add current sample word to ringbuffer
    if( sgRingBuffer.CanWrite() )   // if space in ringbuffer
    {
      int16_t  total = 0;

      total += gBD.NextSample();
      total += gBG.NextSample();
      total += gCL.NextSample();
      total += gCW.NextSample();
      total += gCY.NextSample();
      total += gGU.NextSample();
      total += gMA.NextSample();
      total += gQU.NextSample();

      if( total < -127 )  total = -127;
      if( total >  127 )  total =  127;                           
      cli();
      sgRingBuffer.Write( total+128 );
      sei();
 
      //----------------------------------------------------------------------- sequencer block
      if( digitalReadFast( 10 ) )  // RUN/STOP switch
        gCurrentPattern->Run();
      else
        gCurrentPattern->Stop();

      //----------------------------------------------------------------------- ADC block
      if( !(ADCSRA & 64) ) // AD conversion finished
      {
        uint16_t  value = ((ADCL+(ADCH<<8))>>3)+1;
        if( MUX == 4 )  gCurrentPattern = gPatterns[ (value-1)>>3 ];
        if( MUX == 5 )  ::nEM::gTempo = (value<<4)+1250;  //17633-1250

        MUX++;
        if( MUX == 6 )  MUX = 4;
        ADMUX = 64 | MUX; // Select MUX
        sbi( ADCSRA, ADSC ); //start next AD conversion
      }
    } // if can write
  } // for
}


} // namespace  nO2MiniPop


//---------------------------------------------------------------------------------------------------------------------------


ISR( TIMER1_COMPA_vect )
{
    if( ::nO2MiniPop::sgRingBuffer.CanRead() )    // If entry in FIFO..
        OCR2A = ::nO2MiniPop::sgRingBuffer.Read();  // Output LSB of 16-bit DAC
}


void  setup()
{
  ::nO2MiniPop::Setup();
}


void  loop()
{
  ::nO2MiniPop::Loop();
}

