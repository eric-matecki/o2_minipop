// O2 Minipops rhythm box (c) DSP Synthesizers 2016
// Free for non commercial use

// http://janostman.wordpress.com

// cleaned up by Eric Matecki 2018

#ifdef __MACOSX__
#include "avrX/interrupt.h"
#include "avrX/io.h"
#include "avrX/pgmspace.h"
#else
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#endif

#include "EM.Patterns.h"
#include "O2MiniPop.Samples.h"


namespace  nO2MiniPop {


extern  const ::nEM::cPattern  kPattern_HardRock;
extern  const ::nEM::cPattern  kPattern_Disco;
extern  const ::nEM::cPattern  kPattern_Reegae;
extern  const ::nEM::cPattern  kPattern_Rock;
extern  const ::nEM::cPattern  kPattern_Samba;
extern  const ::nEM::cPattern  kPattern_Rumba;
extern  const ::nEM::cPattern  kPattern_ChaCha;
extern  const ::nEM::cPattern  kPattern_Swing;
extern  const ::nEM::cPattern  kPattern_Nova;
extern  const ::nEM::cPattern  kPattern_Beguine;
extern  const ::nEM::cPattern  kPattern_SynthPop;
extern  const ::nEM::cPattern  kPattern_Boogie;
extern  const ::nEM::cPattern  kPattern_Waltz;
extern  const ::nEM::cPattern  kPattern_JazzRock;
extern  const ::nEM::cPattern  kPattern_SlowRock;
extern  const ::nEM::cPattern  kPattern_Oxygen;


extern  ::nEM::cPatternPlayer  gHardRock;
extern  ::nEM::cPatternPlayer  gDisco;
extern  ::nEM::cPatternPlayer  gReegae;
extern  ::nEM::cPatternPlayer  gRock;
extern  ::nEM::cPatternPlayer  gSamba;
extern  ::nEM::cPatternPlayer  gRumba;
extern  ::nEM::cPatternPlayer  gChaCha;
extern  ::nEM::cPatternPlayer  gSwing;
extern  ::nEM::cPatternPlayer  gNova;
extern  ::nEM::cPatternPlayer  gBeguine;
extern  ::nEM::cPatternPlayer  gSynthPop;
extern  ::nEM::cPatternPlayer  gBoogie;
extern  ::nEM::cPatternPlayer  gWaltz;
extern  ::nEM::cPatternPlayer  gJazzRock;
extern  ::nEM::cPatternPlayer  gSlowRock;
extern  ::nEM::cPatternPlayer  gOxygen;


extern  ::nEM::cPatternPlayer*  gPatterns[16];


extern  ::nEM::cPatternPlayer*  gCurrentPattern;


} // namespace  nO2MiniPop

