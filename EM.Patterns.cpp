// (c) Eric Matecki 2018


#include "EM.Patterns.h"

#include "Arduino.h"

#ifdef __MACOSX__
#include "avrX/interrupt.h"
#include "avrX/io.h"
#include "avrX/pgmspace.h"
#else
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#endif


namespace  nEM {


//BEURK: varglob !!!
uint16_t  gTempo = 3500;


} // namespace  nEM

