// O2 Minipops rhythm box (c) DSP Synthesizers 2016
// Free for non commercial use

// http://janostman.wordpress.com

// cleaned up by Eric Matecki 2018

#ifdef __MACOSX__
#include "avrX/interrupt.h"
#include "avrX/io.h"
#include "avrX/pgmspace.h"
#else
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#endif

#include "EM.Samples.h"


namespace  nO2MiniPop {


extern  ::nEM::cSamplePlayer  gBD;
extern  ::nEM::cSamplePlayer  gBG;
extern  ::nEM::cSamplePlayer  gCL;
extern  ::nEM::cSamplePlayer  gCW;
extern  ::nEM::cSamplePlayer  gCY;
extern  ::nEM::cSamplePlayer  gGU;
extern  ::nEM::cSamplePlayer  gMA;
extern  ::nEM::cSamplePlayer  gQU;


} // namespace  nO2MiniPop

