#ifndef __EM_Samples_HH
#define __EM_Samples_HH

// O2 Minipops rhythm box (c) DSP Synthesizers 2016
// Free for non commercial use

// http://janostman.wordpress.com

// cleaned up by Eric Matecki 2018

#ifdef __MACOSX__
#include "avrX/interrupt.h"
#include "avrX/io.h"
#include "avrX/pgmspace.h"
#else
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#endif


namespace  nEM {


//TODO: SoundSample ??
//TODO: Signed & Unsigned versions ??


struct  cSample
{
  const uint16_t  mCount;
  const uint8_t*  mSamples;
};


#define  SAMPLE_BEGIN( iName )  static  const uint8_t  iName##Bytes[] PROGMEM =
#define  SAMPLE_END( iName )    ; const ::nEM::cSample  iName = { sizeof(iName##Bytes), iName##Bytes };


//TODO: get rid of mCount & add mPointerEnd (use mSamplesCount??)
class  cSamplePlayer
{
public:
  cSamplePlayer( const cSample&  iSample ) :
    mSamples( iSample.mSamples ),
    mSamplesCount( iSample.mCount ),
    mPointer( 0 ),
    mCount( 0 )
  {
  }

  inline  int8_t  NextSample()
  {
    if( mCount )
    {
      mCount--;
      return  pgm_read_byte_near( mSamples + mPointer++ ) - 128;
    }
    return  0;
  }

  inline  void  Start() //NAME: StartPlay()
  {
    mPointer = 0;
    mCount = mSamplesCount;
  }

private:
  const uint8_t*  mSamples;
  const uint16_t  mSamplesCount;

  uint16_t  mPointer;
  uint16_t  mCount;
};


} // namespace  nEM

#endif // __EM_Samples_HH
