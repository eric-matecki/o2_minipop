// O2 Minipops rhythm box (c) DSP Synthesizers 2016
// Free for non commercial use

// http://janostman.wordpress.com

// cleaned up by Eric Matecki 2018

#ifdef __MACOSX__
#include "avrX/interrupt.h"
#include "avrX/io.h"
#include "avrX/pgmspace.h"
#else
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#endif


namespace  nEM {


class  cRingBuffer
{
public:
  cRingBuffer() :
    // mBuffer[256];
    mWrite( 0 ),
    mRead( 0 ),
    mCount( 0 )
  {
  }

  inline  bool  CanWrite() const
  {
    return  mCount < 255;
  }

  inline  void  Write( uint8_t  iValue )
  {
    mBuffer[mWrite++] = iValue;
    mCount++;
  }

  inline  bool  CanRead() const
  {
    return  mCount;
  }

  inline  uint8_t  Read()
  {
    mCount--;
    return  mBuffer[mRead++];
  }

private:
  uint8_t  mBuffer[256];
  uint8_t  mWrite;
  uint8_t  mRead;
  volatile uint8_t  mCount;
};


} // namespace  nEM
