// O2 Minipops rhythm box (c) DSP Synthesizers 2016
// Free for non commercial use

// http://janostman.wordpress.com

// cleaned up by Eric Matecki 2018

#include "Arduino.h"

#ifdef __MACOSX__
#include "avrX/interrupt.h"
#include "avrX/io.h"
#include "avrX/pgmspace.h"
#else
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#endif

#include "O2MiniPop.Patterns.h"


namespace  nO2MiniPop {


//Patterns GU BG2 BD CL CW MA CY QU

PATTERN_BEGIN( kPattern_HardRock )
{
  16,

  B00101100,
  B00000000,
  B00000100,
  B00000000,
  B00101110,
  B00000000,
  B00100100,
  B00000000,
  B00101100,
  B00000000,
  B00000100,
  B00000000,
  B00101110,
  B00000000,
  B00000100,
  B00000000
}
PATTERN_END( kPattern_HardRock )

PATTERN_BEGIN( kPattern_Disco )
{
  16,

  B00100100,
  B00000000,
  B00000100,
  B00010100,
  B00100110,
  B00000000,
  B00000001,
  B00000100,
  B00100100,
  B00000000,
  B00000100,
  B00000100,
  B01100110,
  B00000100,
  B01000001,
  B00000000
}
PATTERN_END( kPattern_Disco )

PATTERN_BEGIN( kPattern_Reegae )
{
  16,

  B01000001,
  B00000100,
  B10000000,
  B00000000,
  B00010110,
  B00000000,
  B10010000,
  B00000000,
  B00100000,
  B00000000,
  B10010000,
  B00000000,
  B00000110,
  B00000000,
  B10000100,
  B00000000
}
PATTERN_END( kPattern_Reegae )

PATTERN_BEGIN( kPattern_Rock )
{
  16,

  B00100100,
  B00000000,
  B00000100,
  B00000000,
  B00000110,
  B00000000,
  B00100100,
  B00000000,
  B00100100,
  B00000000,
  B00000100,
  B00000000,
  B00000110,
  B00000000,
  B00000110,
  B00000000
}
PATTERN_END( kPattern_Rock )

PATTERN_BEGIN( kPattern_Samba )
{
  16,

  B10110101,
  B00010100,
  B10000100,
  B00010100,
  B10110100,
  B00000100,
  B01000100,
  B10010100,
  B00100100,
  B10010100,
  B01000100,
  B10010100,
  B10110101,
  B00000100,
  B10010100,
  B00000100
}
PATTERN_END( kPattern_Samba )

PATTERN_BEGIN( kPattern_Rumba )
{
  16,

  B00100110,
  B00000100,
  B00000001,
  B00110100,
  B00100100,
  B00000001,
  B00010110,
  B00000100,
  B00100100,
  B00000100,
  B00010001,
  B00100100,
  B00110100,
  B00000100,
  B01000001,
  B00000100
}
PATTERN_END( kPattern_Rumba )

PATTERN_BEGIN( kPattern_ChaCha )
{
  16,

  B00100100,
  B00000000,
  B00000000,
  B00000000,
  B00000110,
  B00000000,
  B01000000,
  B00000000,
  B00100100,
  B00000000,
  B00000010,
  B00000000,
  B01000101,
  B00000000,
  B00000010,
  B00000000
}
PATTERN_END( kPattern_ChaCha )

PATTERN_BEGIN( kPattern_Swing )
{
  16,

  B00100100,
  B00000000,
  B00000000,
  B00000000,
  B00000100,
  B00000000,
  B00000000,
  B00000100,
  B00000100,
  B00000000,
  B00000000,
  B00000000,
  B00000100,
  B00000000,
  B00000000,
  B00000100
}
PATTERN_END( kPattern_Swing )

PATTERN_BEGIN( kPattern_Nova )
{
  16,

  B00100001,
  B00000100,
  B00000100,
  B00100100,
  B00100001,
  B00000100,
  B01000100,
  B00000100,
  B00100001,
  B00000100,
  B00000100,
  B00100000,
  B00100001,
  B01000101,
  B00000100,
  B00000100
}
PATTERN_END( kPattern_Nova )

PATTERN_BEGIN( kPattern_Beguine )
{
  16,

  B00100110,
  B00000000,
  B00000001,
  B00000000,
  B00000100,
  B00000000,
  B01100110,
  B00000000,
  B00100100,
  B00000000,
  B01000100,
  B00000100,
  B00100110,
  B00000000,
  B00000100,
  B00000000
}
PATTERN_END( kPattern_Beguine )

PATTERN_BEGIN( kPattern_SynthPop )
{
  16,

  B10100000,
  B00000000,
  B10100010,
  B00000000,
  B00100000,
  B00000000,
  B00100110,
  B00000100,
  B01100000,
  B00000000,
  B01100110,
  B00000100,
  B00100000,
  B00000000,
  B00100010,
  B10001000
}
PATTERN_END( kPattern_SynthPop )

PATTERN_BEGIN( kPattern_Boogie )
{
  12,

  B00100000,
  B00000000,
  B00100100,
  B00000110,
  B00000000,
  B00100100,
  B00100100,
  B00000000,
  B00100100,
  B00000110,
  B00000000,
  B00100100
}
PATTERN_END( kPattern_Boogie )

PATTERN_BEGIN( kPattern_Waltz )
{
  12,

  B00100100,
  B00000000,
  B00000000,
  B00000000,
  B00010010,
  B00000000,
  B00000000,
  B00000000,
  B00010010,
  B00000000,
  B00000000,
  B00000000
}
PATTERN_END( kPattern_Waltz )

PATTERN_BEGIN( kPattern_JazzRock )
{
  12,

  B00100110,
  B00000000,
  B00000100,
  B00000000,
  B00000110,
  B00000000,
  B00000100,
  B00000000,
  B00000110,
  B00000000,
  B01100000,
  B00000000
}
PATTERN_END( kPattern_JazzRock )

PATTERN_BEGIN( kPattern_SlowRock )
{
  12,

  B00100100,
  B00000000,
  B00000100,
  B00000000,
  B00000100,
  B00000000,
  B00000110,
  B00000000,
  B00000100,
  B00000000,
  B00100100,
  B00000000
};
PATTERN_END( kPattern_SlowRock )

PATTERN_BEGIN( kPattern_Oxygen )
{
  12,

  B00100101,
  B00001100,
  B00000100,
  B00101110,
  B00000100,
  B00010100,
  B00100101,
  B00000100,
  B00000100,
  B00101100,
  B00000100,
  B11100100
}
PATTERN_END( kPattern_Oxygen )


//Patterns GU BG BD CL CW MA CY QU (from bit 7 to bit 0)
static  ::nEM::cSamplePlayer*  sgSamplePlayers[] =
{
  &::nO2MiniPop::gQU,
  &::nO2MiniPop::gCY,
  &::nO2MiniPop::gMA,
  &::nO2MiniPop::gCW,
  &::nO2MiniPop::gCL,
  &::nO2MiniPop::gBD,
  &::nO2MiniPop::gBG,
  &::nO2MiniPop::gGU
};


::nEM::cPatternPlayer  gHardRock( kPattern_HardRock, sgSamplePlayers );
::nEM::cPatternPlayer  gDisco   ( kPattern_Disco   , sgSamplePlayers );
::nEM::cPatternPlayer  gReegae  ( kPattern_Reegae  , sgSamplePlayers );
::nEM::cPatternPlayer  gRock    ( kPattern_Rock    , sgSamplePlayers );
::nEM::cPatternPlayer  gSamba   ( kPattern_Samba   , sgSamplePlayers );
::nEM::cPatternPlayer  gRumba   ( kPattern_Rumba   , sgSamplePlayers );
::nEM::cPatternPlayer  gChaCha  ( kPattern_ChaCha  , sgSamplePlayers );
::nEM::cPatternPlayer  gSwing   ( kPattern_Swing   , sgSamplePlayers );
::nEM::cPatternPlayer  gNova    ( kPattern_Nova    , sgSamplePlayers );
::nEM::cPatternPlayer  gBeguine ( kPattern_Beguine , sgSamplePlayers );
::nEM::cPatternPlayer  gSynthPop( kPattern_SynthPop, sgSamplePlayers );
::nEM::cPatternPlayer  gBoogie  ( kPattern_Boogie  , sgSamplePlayers );
::nEM::cPatternPlayer  gWaltz   ( kPattern_Waltz   , sgSamplePlayers );
::nEM::cPatternPlayer  gJazzRock( kPattern_JazzRock, sgSamplePlayers );
::nEM::cPatternPlayer  gSlowRock( kPattern_SlowRock, sgSamplePlayers );
::nEM::cPatternPlayer  gOxygen  ( kPattern_Oxygen  , sgSamplePlayers );


::nEM::cPatternPlayer*  gPatterns[16] =
{
  &gHardRock, &gDisco,   &gReegae,   &gRock,   &gSamba, &gRumba,    &gChaCha,   &gSwing,
  &gNova,     &gBeguine, &gSynthPop, &gBoogie, &gWaltz, &gJazzRock, &gSlowRock, &gOxygen
};


::nEM::cPatternPlayer*  gCurrentPattern = gPatterns[13];


} // namespace  nO2MiniPop

